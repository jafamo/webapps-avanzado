
//A.- Filtrar los elementos de un number que son > 0

let col: number[] = [-3, -2, -1, 0 , 1, 2, 3, 4];
let ret: number[] = [];
for(let valor of col){
    if ( valor > 0){
        ret.push(valor);
    }
}
console.log(ret);

//B.- Filtrar un number[] por el valor de retorno de una función

let col: number[] = [-3, -2, -1, 0 , 1, 2, 3, 4];
let ret: number[] = [];
function fn(x) { return x>0}

for(let valor of col){
    if(fn(valor) ){
        ret.push(valor);
    }
}
console.log(ret);

//C Definir función filter() que filtre un number[] por el valor de retorno de una función
