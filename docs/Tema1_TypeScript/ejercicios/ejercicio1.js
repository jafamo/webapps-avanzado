//A.- Filtrar los elementos de un number que son > 0
var col = [-3, -2, -1, 0, 1, 2, 3, 4];
var ret = [];
for (var _i = 0, col_1 = col; _i < col_1.length; _i++) {
    var valor = col_1[_i];
    if (valor > 0) {
        ret.push(valor);
    }
}
console.log(ret);
//B.- Filtrar un number[] por el valor de retorno de una función
var col = [-3, -2, -1, 0, 1, 2, 3, 4];
var ret = [];
function fn(x) { return x > 0; }
for (var _a = 0, col_2 = col; _a < col_2.length; _a++) {
    var valor = col_2[_a];
    if (fn(valor)) {
        ret.push(valor);
    }
}
console.log(ret);
//C Definir función filter() que filtre un number[] por el valor de retorno de una función
