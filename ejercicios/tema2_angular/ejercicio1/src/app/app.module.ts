import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { TasksListComponent } from './tasks-list/tasks-list.component';
import { TaskDetailsComponent } from './task-details/task-details.component';

// importar las animaciones del ejercicio 4
//import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatButtonModule} from "@angular/material/button";
import {MatIconModule} from "@angular/material/icon";
import {MatListModule} from "@angular/material/list";
import {TasksService} from "./tasks.service";
import { AddTaskComponent } from './add-task/add-task.component';
import { EditTaskComponent } from './edit-task/edit-task.component';

import { RouterModule} from '@angular/router'; //ejercicio 6
import { MatFormFieldModule } from '@angular/material/form-field';//ejercicio 6
import { MatInputModule } from '@angular/material/input';//ejercicio 6
import { FormsModule } from '@angular/forms';//ejercicio 6


@NgModule({
  declarations: [
    AppComponent,
    TasksListComponent,
    TaskDetailsComponent,
    AddTaskComponent,
    EditTaskComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatToolbarModule, //ejercicio 4 para la barra de menu
    MatButtonModule, //ejercicio 4 para los botones
    MatIconModule, //ejercicio 4
    MatListModule, //ejercicio 4
    MatFormFieldModule, //ejercicio 6
    MatInputModule,//ejercicio 6
    FormsModule, //ejercicio 6
    RouterModule.forRoot(
      [
      {
        path: 'tasks', component: TasksListComponent
      },
      {
        path: 'add', component: AddTaskComponent
      },
      {
        path: 'edit/:id', component: EditTaskComponent
      },
      {
        path: '', 
        redirectTo: '/tasks', 
        pathMatch: 'full'
      },
      {path: '**', redirectTo: '/tasks', pathMatch: 'full'}
    ]
     // { enableTracing: true } // <-- debugging purposes only
    )

  ],
  providers: [TasksService], // ejercicio 5
  bootstrap: [AppComponent]
})
export class AppModule { }
