import { Injectable } from '@angular/core';
import {Task} from "./task";

@Injectable()

export class TasksService {
  public id = 0;
  public tasks: Task[] = [];

  constructor() {
    if(localStorage.getItem('id')){
      this.id = Number(localStorage.getItem('id'));
    }      

    if(localStorage.getItem('tasks')){
      this.tasks = JSON.parse(localStorage.getItem('tasks'));
    }
   }

  //mostrar todas las tareas
  findAllTasks(): Task[] {
    return this.tasks;
  }

  //buscar una tarea por Id
  searchTaskById(id:number): Task{
    let index = this.tasks.findIndex((t) => t.id === id );
    if(index !== -1) return this.tasks[index];
    else return null;
  }

  //añadir una tarea
  addTask(t: Task): void{
    //incrementamos el id
    t.id = this.id++;
    this.tasks.push(t);
    localStorage.setItem('id', String(this.id));
    localStorage.setItem('tasks', JSON.stringify(this.tasks));

  }
  //modificar tarea
  modifyTask(t: Task): void{
    let index = this.tasks.findIndex( (_t) => _t.id === t.id );
    if(index !== -1 ){
      this.tasks[index] = t;
      localStorage.setItem('tasks', JSON.stringify(this.tasks));
    }else{
      console.log("modifyTask not find index");
    }


  }
  //eliminar tarea
  removeTask(id: number): void{
    let index = this.tasks.findIndex((t) => t.id === id );
    if(index !== 1){
      this.tasks.splice( index, 1);
      localStorage.setItem('tasks', JSON.stringify(this.tasks));
    }
  }



}
