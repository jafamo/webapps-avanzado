import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TasksService } from '../tasks.service';
import { Task } from '../task';
import { Location } from '@angular/common';

@Component({
  selector: 'app-edit-task',
  templateUrl: './edit-task.component.html',
  styleUrls: ['./edit-task.component.css']
})
export class EditTaskComponent implements OnInit {

  public task: Task;

  constructor(
    public route: ActivatedRoute,
    public tasks: TasksService,
    public location: Location
  ) { }

  back(){
    this.tasks.modifyTask(this.task);
    this.location.back();
  }
  completeTask() {
    
    this.task.pending = false;
    this.tasks.modifyTask(this.task);
    this.location.back();
  }

  deleteTask() {
    this.tasks.removeTask(this.task.id);
    this.location.back();
  }

  ngOnInit(): void {
    //obtener el ID por get
    let id = Number(this.route.snapshot.paramMap.get('id'));
    console.log("Este es el id:"+id);
    this.task = this.tasks.searchTaskById(id);

  }

}
