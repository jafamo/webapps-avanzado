import { Component, OnInit } from '@angular/core';
import { Task } from '../task';
import {TasksService} from "../tasks.service";
import { Router } from '@angular/router';

@Component({
  selector: 'app-tasks-list',
  templateUrl: './tasks-list.component.html',
  styleUrls: ['./tasks-list.component.css']
})
export class TasksListComponent implements OnInit {

  // construir de forma dinaimca
  //public id=0;
  //public tasks: Task[] = [];


  constructor( public tasks: TasksService, public router:Router) {

    /*this.tasks = [
      {id: 1, title: 'Tarea 1', pending: false},
      {id: 2, title: 'Tarea 2', pending: false},
      {id: 3, title: 'Tarea 3', pending: true}
    ]*/
  }

  deleteTask(tasks){
    //this.tasks.splice(this.tasks.findIndex((t)=> tasks.id === t.id),1)
    this.tasks.removeTask(tasks.id);

  }
  //ejercicio 4, añadir nuevas tareas con identificado id++
  addTask(){
    this.router.navigateByUrl('/add');
    //let title: string = prompt("Please enter task name");
    //this.tasks.addTask({title: title, pending: true});
    /*
    this.tasks.push({
    id: this.id++,
      title: title,
      pending: true
    });
     */
  }

  ngOnInit(): void {
  }

}
